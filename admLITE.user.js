// ==UserScript==
// @name         adminka lite
// @description  Ништяки для саппорта в серверной админке
// @namespace    adminka-lite-masterwind
// @author       masterwind
// @version      21.3.10
// @updateURL    https://admlite.netlify.app/admLITE.user.js
// @downloadURL  https://admlite.netlify.app/admLITE.user.js
// @supportURL   https://bitbucket.org/masterwind/admlite/issues/
// @include      https://*.ucozmedia.com/cgi/uAdmin/*
// @include      https://*.ucoz.net/cgi/uAdmin/*
// @include      https://*.uweb.ru/cgi/uAdmin/*
// @icon         https://admlite.netlify.app/logo.png
// @require      https://utils-masterwind.netlify.app/utils.js
// @grant        none
// @noframes
// ==/UserScript== */


;((app, stp, CSS = [], INSTANCES = [] ) => {
stp.debug && console.log(window, app, stp, CSS, INSTANCES );


	//// ОБЩИЕ ФУНКЦИИ НА СТРАНИЦАХ СО СПИСКОМ СЕРВЕРОВ = uServers
stp.debug && window.servers && !console.log(`window.servers`, servers, /s\d+/.test(servers.textContent) ) && INSTANCES.push('uServers');
	window.servers && /s\d+/.test(servers.textContent) && CSS.push('global') && (
		/// подсветка випов
		stp.vipServer.map((numServ ) => (vip = document.querySelector(`#servers a[href*="s${numServ}"]`)) && vip.classList.add('vip')),
		/// детектим сайты на странице
		(userSites = document.querySelectorAll('.user-site')),
		/// форма поиска сайтов
		(searchForm = (document.search_form || window['search-form'])) && (  //&& !SEARCHFORM.setAttribute('onsubmit', `app.searchSubmit()`)
			// отсеять ненужные таблицы сайтов
			userSites && userSites.length > 3 && !!searchForm.elements['search-input'].value && window['search-select'].insertAdjacentHTML('beforebegin',
				`<a href=javascript:;
					onclick="CS=searchForm.elements['search-input'].value.trim(); for(let uSite of userSites) uSite.querySelector('input[id^=user-domain]').value.includes(CS) && uSite.remove();">oтсеять</a>`),
			// кнопки вместо селекта
			Object.values(searchForm['search-select']).map(OPT => window['search-var'].parentElement.insertAdjacentHTML('beforeend',
				`<input value=${OPT.textContent} type=button onclick=admlite.searchSubmit(this) data-params='{"value":"${OPT.value}","name":"${OPT.textContent}","id":"${OPT.id}"}' > `))
		),
		/// апгрейдим таблицы сайтов
		searchForm && userSites.length && CSS.push('users') && !userSites.forEach((
			uSite, index, all,
			siteId          = (uSite.id && uSite.id.match(/\d+/) || uSite.querySelector(`a[href*=payctrl]`).getAttribute('href').match(/u=(\d+)/)).pop(),
			siteHost        = (sHost = uSite.querySelector('input[id^="user-domain"], td > b')) && sHost.value || sHost.textContent,
			sitePro         = uSite.querySelector('.pro > .red, font[title=Pro][color=red]'),
			sitePhone       = uSite.querySelector('.fa.green, font[color=green]'),
			siteUnconfirmed = uSite.querySelector('.fa.gray, font[color=silver]'),
		) => {
			// раскрас по типу
			(sitePro && uSite.classList.add('is-pro')) || (sitePhone && uSite.classList.add('has-phone')) || (siteUnconfirmed && uSite.classList.add('not-confirm'));
			// допишем адрес сайта к окну лога для дальнейшей постройки FTP-ссылки
			(logLink = uSite.querySelector('a[onclick*="l=log"][title*=Log]')) && logLink.setAttribute('onclick',
				`window.open('?l=log&u=${siteId}', 'log::${siteHost.trim()}', 'resizable=1,scrollbars=1,width=900,height=screen.height*.9,top=10,left=10' ); return false;` );
		}),
		/// отобразить авторизации & траффик одного сайта за неделю
		(stp.showTraff > 0 && stp.showTraff <= 10 )
		&& utils.location.has('user=') && utils.location.has('l=fu') && (userSites.length > 0 && userSites.length <= 3) && CSS.push('traff') && (
			(siteId = document.querySelector('.sel')) && (siteId = siteId.value) && !document.body.insertAdjacentHTML('beforeend',
				`<div data-CHEAT><img src="/img/icon/del.png" onclick="this.parentElement.remove();"><table id=traffWrapper></table><table id=authWrapper></table></div>`),
			// крайние N успешных входов в ПУ
			self.fetch && window.authWrapper && fetch(location.origin + location.pathname + '?l=log&u=' + siteId, {credentials:'include', } )
				.then(RES => RES.text(), ERR => ERR )
				.then((data, DOM = utils.convert.domParse(data) ) => {
					authWrapper.insertAdjacentElement('beforeend', DOM.querySelector('table tr') );
					for (let [index, value ] of DOM.querySelectorAll('table tr.uLogin').entries() ) {
						if (index >= stp.showLogin ) break;
						authWrapper.insertAdjacentElement('beforeend', value);
					}
				}),
			// крайние N дней траффика
			window.fetch && window.traffWrapper && fetch(location.origin + location.pathname + '?l=sitetraf&u=' + siteId, {credentials:'include', } )
				.then(RES => RES.text(), ERR => ERR )
				.then((data, DOM = utils.convert.domParse(data) ) => {
					for (let [index, value ] of DOM.querySelectorAll('table tr').entries() ) {
						if (index > stp.showTraff ) break;
						traffWrapper.insertAdjacentElement('beforeend', value);
					}
				})
		)
	);


	//// ФИКС ТАБЛИЦЫ МОДЕРАТОРОВ = uModersList
stp.debug && !console.log(`pageIs(l=moders)`, utils.location.has('l=moders') ) && INSTANCES.push('uModersList');
	utils.location.has('l=moders') && CSS.push('moders') && (
		cont.classList.add('al-moders'),
		utils.location.has('act=2') && cont.classList.add('al-moders-edit'),
		cont.insertAdjacentHTML('afterbegin', '<h1>Moderators</h1>')
	);


	// ДОБАВИМ СПИСОК ЗАБАНЕННЫХ IP ПОД МЕНЮ = uBannedIP
stp.debug && !console.log(`includes(.tmenu) && !pageIs(l=banips) && !includes(input[value=':: Login ::'])`, (document.querySelector('.tmenu') && !utils.location.has('l=banips') && !document.querySelector("input[value=':: Login ::']")) ) && INSTANCES.push('uBannedIP');
	document.querySelector('.tmenu') && !utils.location.has('l=banips') && !document.querySelector("input[value=':: Login ::']")
		&& (bannedIp = document.querySelector('.tmenu a[href*="l=banips"]'))
		&& !bannedIp.addEventListener('click', (event ) => {
			// !document.getElementById('bannedIP') && self.fetch && fetch(`${location.origin}${location.pathname}?l=banips`, {credentials:'include', } )
			!window.bannedIP && self.fetch && fetch(`${location.origin + location.pathname}?l=banips`, {credentials:'include', } )
				.then(result => result.text(), error => error ).then((data, DOM = utils.convert.html2DOM(data) ) => {
					// добавим блок в который поместим IP
					window.side.insertAdjacentHTML('beforeend', `<div id="bannedIP">${DOM.getElementById('cont').innerHTML.replace(/^(List.+IPs:)(.+)$/, '<b>$1</b>$2')}</div>`);
					!!stp.showTraff && self.traffWrapper && traffWrapper.parentElement.remove();
				});
		}) && bannedIp.setAttribute('href', '#');


	//// ОБЩИЙ ПЕРЕКЛЮЧАТЕЛЬ СОСТОЯНИЙ ПО НАЗВАНИЮ ОКНА = winNameSwitch
stp.debug && !console.log(`window.name`, window.name ) && INSTANCES.push('winNameSwitch');
	switch (window.name ) {
		// Кнопка для отключения попандера
		case 'bg': CSS.push('bangroup') && (action = 'document.forms[1].popunder.value = this.value; document.forms[1].submit();')
			!document.forms[1].popunder.insertAdjacentHTML('beforeBegin', `<br><button type=button onclick="${action}" value=0>Вкл.</button> ` )
			!document.forms[1].popunder.insertAdjacentHTML('afterEnd', ` <button type=button id="off" onclick="${action}" value=1>Выкл.</button> ` )
			off.focus(); break;
		// Изменяем размер окна PayM
		case 'stp2': window.resizeTo(390, (screen.height - 100)); break;
		// case '': break;
		// закрыть окно восстановления/удаления сайта
		case 'deluser':
		case 'restuser':
		case 'msgwnd':
		case 'restore': CSS.push('msgwnd') && setTimeout("window.close()", stp.timeout.closeWindow ); break;
	}


	//// ССЫЛКИ В СПИСКЕ ДОМЕНОВ = uDomains
stp.debug && !console.log(`pageIs(domains|bldomains)`, utils.location.has('l=domains'), utils.location.has('l=bldomains') ) && INSTANCES.push('uDomains');
	(utils.location.has('l=domains') || utils.location.has('l=bldomains') ) && CSS.push('domains') && (
		cont.classList.add('al-domains'),
		cont.firstElementChild.classList.add('al-paging'),
		cont.insertAdjacentHTML('afterbegin', '<h1>Domains</h1>'),
		Array.from(document.forms).map(form => {
			form.user[0] && form.user[0].setAttribute('type', 'text' ) || form.user.size && form.user.setAttribute('type', 'text' );
			if (form.dom ) return;
			form.user[0].insertAdjacentHTML('afterEnd',
				`<button onclick="window.open('admin.cgi?l=fu&user=${form.user[0].value}');" class=al-domains-2canonical title="Перейти к сайту" type=button>&raquo;</button>`);
			(domMatch = form.domain.value.trim().match(/^.+?((?:\.\w{2,3})?(?:\.\w{2,4}(?:--[0-9a-z]+)?))$/)) && (domZone = domMatch.pop() || 'default');
			form.domain.insertAdjacentHTML('afterEnd',
				`<button onclick="window.open('//${stp.whoisURL[domZone] || stp.whoisURL['default']}${form.domain.value}')" class=al-domains-2whois title="Посмотреть WHOIS" type=button>&raquo;</button>`);
		})
	);


	//TODO REFACTOR LATER
	//// ПЛАТЕЖИ И УСЛУГИ САЙТА = uSitePayFlows
stp.debug && !console.log(`pageIs(l=payctrl)`, utils.location.has('l=payctrl'), !!(document.forms.length && (document.forms[0].curamount || document.forms[0].cuamount)) ) && INSTANCES.push('uSitePayFlows');
	utils.location.has('l=payctrl') && CSS.push('payctrl')
	// форма конвертера
	&& (CONV = document.forms[0]) && !!CONV.curamount
		&& !CONV.classList.add('converter', 'new-forms') && (CONV.pm.value = '!840') && !CONV.elements[2].insertAdjacentHTML('beforebegin',
			`<input type="button" value="Списать с баланса" id="fromBalance" onclick="CONV.curamount.value = -${document.querySelector('.user-site').textContent.trim().match(/:\s(?:\$|[y\.e]+)([\d\.]+)$/)[1]}"> `)
		&& stp.uPackages.map(PACK => CONV.insertAdjacentHTML('beforeend',
			`<span onclick="document.forms[0].curamount.value = ${PACK[1]};">${PACK[2]}$ на год - ${PACK[1]}</span>`))
		&& document.querySelectorAll('.orderTd').forEach(TD => /^\$|^y\.e\./.test(TD.textContent.trim()) && TD.addEventListener('click', app.amount2converter))
		;
	// форма нового платежа
	utils.location.has('l=payctrl') && (NEWPAY = document.forms[+!utils.location.has('s=fl')])
	&& !NEWPAY.classList.add('newPay', 'new-forms', `form-${+utils.location.has('s=fl')}`) && (NEWPAY.flowcat.checked = !0) && !NEWPAY.cuamount.setAttribute('onclick', 'this.select()')
		// добавим ссылки быстрого действия
		&& !NEWPAY.details.insertAdjacentHTML('afterEnd',
			`<span data-ALERT></span><span class="fastCOMMENTS"><input type="text" placeholder=comment name="uz_comment" size="50"><br>
${Object.entries(stp.payedLINKS).map(PLINK => `<a href="javascript:" onclick="setPayComment('${PLINK[0]}');" >${PLINK[1][0]}</a>`).join(' &bull; ')}</span><hr>`)
		// обработчик ссылок быстрого действия
		&& (ALERT = NEWPAY.querySelector('[data-ALERT]') )
		&& (window.setPayComment = function(link ) {
			switch (!!NEWPAY.uz_comment.value ) {
				case true : (ALERT.textContent = ' ') && NEWPAY.details
					&& (NEWPAY.details.value = NEWPAY.details.value.replace(/(_UZ_COMMENT:).*$/m, `$1${stp.payedLINKS[link][1].replace('@@@', NEWPAY.uz_comment.value )}${localStorage.moderNAME}`));
					break;
				case false : (ALERT.textContent = stp.payedLINKS[link][2]) && NEWPAY.details && (NEWPAY.details.value = NEWPAY.details.value.replace(/(_UZ_COMMENT:).+$/m, `$1`));
					break;
			}
		});


	//// ОКНO НАСТРОЕК САЙТА - STP = uSiteSetup
stp.debug && !console.log(`pageIs(l=stp)`, utils.location.has('l=stp') ) && INSTANCES.push('uSiteSetup');
	utils.location.has('l=stp') && CSS.push('stp') && (
		window.resizeTo((screen.width < 1280 ? screen.width * .95 : 900), screen.height * .95),
		// !document.head.insertAdjacentHTML('beforeend', `<link href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">`),
		stCode        = document.forms[0].stp,
		submitWrapper = window.sbm.parentElement,
		stArray       = stCode.value.split(/# ---\n\n?/g),
stp.debug && !console.log(`stCode`, stCode ),
stp.debug && !console.log(`stArray`, stArray ),
		// метка рекуррента
		(utils.location.has('m=_st') || !utils.location.has('m=')) && /is_recurrent'?}\s*?=\s*?'?1'?;/g.test(stCode.value)
			&& submitWrapper.insertAdjacentHTML('beforeend', `<button type=button data-recurrent=1> рекуррент</button>`),
		// stCode.focus(),
		// управление блокировкой
		(
stp.debug && !console.log(`stCode.value.includes('AccBaned')`, stCode.value.includes('AccBaned') ),
			sessionStorage.AccBaned && !stCode.value.includes('AccBaned') && (stCode.value = stCode.value.trim()) && (stCode.value += `\n\n${sessionStorage.AccBaned}# ---\n`)
			|| (ABC = stArray.find(item => item.includes('AccBaned'))) && !sessionStorage.setItem('AccBaned', ABC)
			&& !INFO.insertAdjacentHTML('afterbegin',
				`<button
					type=button
					onclick="sessionStorage.AccBaned && (stCode.value = stCode.value.replace(sessionStorage.AccBaned + '# ---', '').trim()); document.forms[0].submit();"
				>Убрать блокировкy</button>`)
		)
	);
/*
		// быстрокоды TODO
		var fastCODES    = {alterLIMITS:'&#8784;', };
		for (var fastITEM in fastCODES ) {
		// 	INFO.innerHTML    += `<button type="button" data-fastCODES="${fastITEM}" ><i class="fa ${fastCODES[fastITEM]}"></i></button>`;
			var FCB = BUTTON.cloneNode();
			FCB.dataset.fastcodes    = fastITEM;
			FCB.innerHTML            = fastCODES[fastITEM];
			INFO.insertAdjacentElement('beforeend', FCB );
		}

		INFO.insertAdjacentHTML('beforeend', ` AlterLimits comming soon...` );
	// 	console.log(STP.val().indexOf('# --- AlterLimits'));
	// 	if (STP.val().indexOf('# --- AlterLimits') === -1) {
	// 		STP.val(function(i, v) {
	// 			return v;
	// 		});
	// /*
	// # --- AlterLimits
	// $setup{'photo_max_height'}=10000;
	// $setup{'news_max_message_size'}=100500;
	// # ---
	// *\/
	// 	}
*/


	//// ЛОГ САЙТА = uSiteLog
stp.debug && !console.log(`pageIs(l=log) && name.includes(log::)`, (utils.location.has('l=log') && window.name.includes('log::')) ) && INSTANCES.push('uSiteLog');
	utils.location.has('l=log') && window.name.includes('log::') && CSS.push('log') && (ftpTab = document.querySelector(`a[onclick^="doFilter('FTP"]`)) && (
		// добавим ивент: при клике по вкладке ftp - отобразить ссылку для просмотра файлов по ftp
		ftpTab.addEventListener('click', (
			event,
			siteId      = +location.href.match(/u=(\d+)$/).pop(), // site ID
			siteName    = window.name.match(/^log:+(.+)$/).pop(), // site name
			userName    = utils.convert.site2user(siteName), // user name
			ftpPassword = (passRow = document.querySelector('tr.uFTP')) && passRow.lastElementChild.innerText, // FTP password
			ftpURI      = ['ftp://', userName.toLowerCase(), ':', ftpPassword, '@', location.hostname ].join(''), // FTP URL
		) =>
			// !console.log(event ) && !console.log('siteId =', siteId, 'siteName =', siteName, 'userName =', userName, 'ftpPassword =', ftpPassword, 'ftpURI =', ftpURI ) &&
			siteId && siteName && userName && ftpPassword && ftpURI && !document.querySelector('[data-ftpURL]') && cont.insertAdjacentHTML('beforeend',
				`<div data-ftpURL>
					<input value="${userName}" onclick="this.select();" type="text">
					<input value="${siteName}" onclick="this.select();" type="text"><!-- ftp://vasya:key@ftp.example.com -->
					<div>${ftpPassword && `<a href="${ftpURI}" target="_blank">${ftpURI}</a>` || 'FTP пароль не установлен. <a href="?l=logasuser&uid=' + siteId + '">Перейти в Панель управления</a> для создания FTP-пароля.'}</div>
				</div>`)
		)
	);


	//// ДОБАВИМ ВСЕ НЕОБХОДИМЫЕ СТИЛИ = customCSS
stp.debug && !console.log(`CSS`, CSS ) && INSTANCES.push('customCSS');
	CSS.length && utils.css.setStyle(CSS.map(type => app.CSS[type]).join('\n\n'));


stp.debug && console.log(`INSTANCES`, INSTANCES );
})(
	// app
	window.admlite = {
		// system CSS
		CSS    : {
			siteusers : `/* SITEUSERS */
body > div.user-site{position:relative; border-radius:0; border:none; border-spacing:0;
	padding:15px; margin:0!important; background:#f8f8f8; }
th{background-color:#e1e1e1!important; }
.findIP:after{content:'\\1F50D'; }`,
			bangroup  : `/* BANGROUP */
form{margin:0 0 10px; }
input[type="submit"]{margin:0 0 0 3px; }`,
			domains   : `/* DOMAINS */
#cont > table, #cont > form table{min-width:75%; border:none!important; margin:0 0 15px; border-spacing:0; width:auto; }
#cont > table td{padding:4px 7px; }
#cont > form{margin:10px 0; width:100%; }
#cont > form table td{padding:4px 7px; }
#cont > div{width:75%; margin:10px auto; }
#cont > div pre{font:9.75pt/1 monospace!important; }
form select{margin:0 10px 0 0; }
form:not(:last-child) input[name=user],
form:not(:last-child) input[name=domain]{border-right:none; border-radius:7px 0 0 7px; margin-right:0; flex-grow:1; }
form input[name=user],
form input[name=domain]{margin:0 0 0 5px; width:180px!important; }
form input[name=domain]{width:290px!important; }
button.al-domains-2canonical, button.al-domains-2whois{border-radius:0 6px 6px 0; height:26px; margin-right:1rem; }
`,
			payctrl   : `/* PAYCTRL */
.unpaidORDERS [onclick^=try], .unpaidORDERS [align=right]{cursor:copy; }
.new-forms{border:1px solid #ccc; box-sizing:border-box; }
.new-forms table{width:100%!important; border-spacing:0; padding:0; }
.new-forms table td{padding:6px 6px 0; }
.new-forms table tr:first-child td{padding:6px; font:bold .9em/1 "Open Sans Light", sans-serif!important; background:#DAE0E0; }
form, form.form-1{margin:0 auto!important; }
form[name="proupdate"] input[type="radio"]{float:left; margin:4px 6px 0 0; }
.converter{width:30%!important; margin:0; min-width:280px; padding:0 0 6px; }
.converter table tr:last-child td {text-align:right; }
.converter input[name="curamount"]{}
.converter hr{border:none; border-top:1px dotted #000; }
.converter > input ~ span{display:block; float:right; margin:0 6px 0 0; cursor:pointer; padding:0 3px; }
.converter > input ~ span:hover{background:#eee; }
.converter + br{display:none; }
input#fromBalance{margin:0 0 5px; }
#cont > table > tbody > tr:last-child > td[align=center]{display:flex; justify-content:space-between; align-items:flex-start; padding:15px 2px 0; }
#cont > table > tbody > tr > td[align=center] > br{display:none; }
.newPay{width:70%!important; margin:0 0 0 15px!important; padding:0 0 1px; }
.newPay textarea{resize:vertical; font:10pt/1.2 monospace; }
.newPay span[data-ALERT]{color:maroon; display:block; text-align:center; margin:7px auto; }
.newPay .fastCOMMENTS + hr{margin:9px 0 3px; }
#bann-controls input[name="comment"]{margin:4px 0; }
.copy{font-weight:bold; }
.copy > input{color:#404246; font:bold 13pt/1 "PT Sans", sans-serif; border:none; background:transparent; }`,
			sysbak    : `/* SYSBAK */
body > div{display:flex; justify-content:space-around; }
body div > br, body div b{display:none; }
body form:first-of-type:before{content:'Allowed Backups :'; }
body form:last-child:before{content:'Saved Backups :'; }
body form:last-child:before,
body form:first-of-type:before{display:block; font-weight:bold; margin:0 0 5px; }
body input[type=submit]{margin:7px 0 0; }`,
			msgwnd    : `/* MSGWND */
body{margin:45px auto 0; }
body center{font:italic 14pt/1 sans!important; }
body b{font:bold italic 14pt/1 sans!important; }
body br{font:1px/0 sans!important; }
body, body center font{font:bold italic 22pt/1 sans; text-align:center; vertical-align:middle; }
body center font{display:block; }`,
			users     : `/* USERS */
.user-site.has-phone{background:linear-gradient(to right, #B6D7A8, #fff); }
.user-site.not-confirm{background:linear-gradient(to right, #B7B7B7, #fff); }
.user-site.not-confirm .fa-phone-square{color:#8F8F8f; }
.user-site.is-pro{background:linear-gradient(to right, #F9CB9C, #fff); }
.user-site.is-pro.has-phone{background:linear-gradient(to right, #F9CB9C, #B6D7A8); }
.user-site.is-pro.not-confirm{background:linear-gradient(to right, #F9CB9C, #B7B7B7); }
.user-site .report i.fa{color:brown; font-size:1em; }
.user-site .report i.fa:before{content:"\\f0f6"; }`,
			log       : `/* LOG */
#servers{text-align:center; }
#servers a{float:none; display:inline-block; }
table{border-collapse:collapse; }
table tr{border-bottom:1px dotted #bbb; }
table tr > th:first-child{width:145px!important; }
table tr > th:nth-child(3){width:110px!important; text-align:center; }
table tr > th:last-child{width:auto!important; }
table tr > td{padding:3px 6px 2px; }
table tr.uLogin td{background:Honeydew; }
table tr.uLoginWRONG td{background:PeachPuff; }
table tr.uFTP td{background:LightBlue; }
table tr.uChangeEMAIL td{background:Khaki; }
table tr.uSecurity td{background:#E4D9FF; }
table tr.uChangePASS td{background:Pink; }
table tr.uChangeOwner td{background:Bisque; }
table tr.uDomain td{background:LemonChiffon; }
table tr.uSending confirmation td{background:#d9d9d9; }
table tr.uPhone was confirmed td{background:Khaki; }
table tr.u(Confirmation phone rejected, Phone already used another) td{background:burlywood; color:brown; }
table tr.u(Deleting account) td{background:#999; color:brown; }
table tr.uDelRESTORE td{background:#ccc;  }
[data-ftpURL]{display:block; margin:1em auto; width:75%; text-align:center; }
[data-ftpURL] a{color:maroon!important; }
[data-ftpURL] input{width:175px; }
[data-ftpURL] div{margin:1em 0 0; }
.green, .green td:nth-child(2){color:green!important; }
.brown, .brown td:nth-child(2){color:brown!important; }`,
			stp       : `/* STP */
[data-RECURRENT="1"]{background:#B6D7A8; border:2px solid darkgreen; }
[data-RECURRENT="0"]{background:#EA9999; border:2px solid maroon; }
[data-RECURRENT]:before{font:normal 12pt/0 FontAwesome; margin:0 5px 0 0; }
[data-RECURRENT="1"]:before{content:'\\f00c'; }
[data-RECURRENT="0"]:before{content:'\\f00d'; }
[data-fastCODES]{margin:0 2px; padding:0; }`,
			global    : `/* GLOBAL */
*{box-sizing:border-box; }
select:focus, select:active{border-color:#1D72DE; }
#servers > .vip{color:brown; }
#servers b:not(.login) { background:#333; }

#side #bannedIP{margin:20px 0 0 20px; }

#cont > br{font:normal 1pt/1 sans; }
#cont .plist b{display:inline-block; min-width:22px; text-align:center; margin:-2px 0; }
#cont .user-site{position:relative; border-radius:0; border:none; border-bottom:1px solid #999; }
#cont .user-site [id^=user-domain]{font:bold 1.05em sans-serif; color:royalblue; }
#cont .user-site .gray{color:#999; }
#cont .user-site .clip-wrap{border:none; background:rgba(127, 127, 127, 0.25); margin:1px 0; }
#cont .user-site .clip-wrap input.clip{border:none; background:transparent; }
#cont .user-site input[title*=Password]{color:green; }
#cont .user-site input[title*="Secret answer"]{color:brown; }
#cont .user-site .user-butt{text-align:right; padding:0 0 0 10px; height:26px; }
#cont .user-site .user-butt a{float:none; }
#cont .user-site .user-butt a i.fa{height:26px; line-height:26px; width:26px; color:#727477; }
#cont .user-site .user-butt a:hover i.fa{font-size:1.75em; vertical-align:middle; }
#cont .user-site .user-butt i.fa.fa-tags,
#cont .user-site .user-butt i.fa.fa-calendar-o{width:30px; height:30px; font-size:1.75em; line-height:30px; }
#cont .user-site td:last-child .user-butt{width:30px!important; }
#cont .user-site td:last-child .user-butt a{display:block; width:28px; height:28px; position:absolute; right:15px; }
#cont .user-site td:last-child .user-butt a[onclick*=summary]{margin:28px 0 0; }
#cont .user-site .user-butt a:hover i.fa.fa-tags,
#cont .user-site .user-butt a:hover i.fa.fa-calendar-o{vertical-align:baseline; }
#cont .user-site .sel{position:absolute; margin:0; bottom:15px; right:15px; }
#cont #search-form{background:#EDF1F2; margin:0; right:0; padding:7px; box-shadow:none; border-right:0; border-bottom:0;
	border-radius:7px 0 0; }
#cont #search-form > div{white-space:nowrap; }
#cont #search-select, #cont #search-submit:not(.list){display:none; }
#search-div textarea{font:normal 1.1em/1 monospace;}
#cont > table{border-spacing:0; padding:15px; margin:0; width:100%; }
`,
			moders : `/* MODERS */
.al-moders{display:flex; flex-wrap:wrap; justify-content:center; }
.al-moders h1{width:100%; }
#cont.al-moders > table{width:50%; }
.al-moders > table:nth-of-type(4n-3),
.al-moders > table:nth-of-type(4n),
.al-moders form{background:#f1f1f1; }
.al-moders > table td{width:auto; }
#cont.al-moders > form > .user-site{border:none; }
.al-moders form{margin:20px; padding:0 15px; border-radius:15px; }
/* remove after refactoring uAdmin */
.al-moders-edit table td,
.al-moders-edit table{border:none!important; background:transparent; margin-bottom:20px; color:#000!important; }
.al-moders-edit table font{color:#404246; }
`,
			traff : `/* TRAFF */
[data-CHEAT]{position:fixed; bottom:7px; left:7px; width:33%; min-width:550px;
	box-sizing:border-box; }
[data-CHEAT] table{width:100%; border:2px solid #404246; border-spacing:1px; background:#fff; margin:7px 0 0; padding:2px; }
[data-CHEAT] table tr:first-child, [data-CHEAT] table tr:first-child td, [data-CHEAT] th{background:#999!important;
	text-align:center; }
[data-CHEAT] td{padding:1px 3px; width:auto!important; }
[data-CHEAT] td, [data-CHEAT] th{font-size:9pt; }
[data-CHEAT] img{position:absolute; top:-4px; right:-11px; background:#fff; padding:2px; border:2px solid #404246;
	border-radius:17px; cursor:pointer; }`,
		},
		// system functions
		amount2converter : (event ) => CONV && (CONV.curamount.value = +event.target.textContent.match(/\d+\.\d+$/)),
		// search form
		searchSubmit : (button,
			PARAMS = button && JSON.parse(button.dataset.params) || {},
			SF = searchForm || window['search-form'],
			QUERY = SF['search-input'],
		) => {
			PARAMS.id && (PARAMS.type = PARAMS.id.match(/[a-z]*$/).toString());
			SF['search-q'] && ([SF['search-q'].name, SF['search-q'].value ] = [PARAMS.value || 'user', [PARAMS.type, QUERY.value.trim()].filter(VAL => !!VAL).join('=') ]);
			SF['search-r'] && (SF['search-r'].value = (PARAMS.id && PARAMS.id.match(/[0-9]/)) || 0);
			SF['search-select'].value = PARAMS.value;
			window['search-div'] && (window['search-div'].className = PARAMS.value);
			!(PARAMS.value == 'list') && SF.submit() || !window['search-submit'].classList.add(PARAMS.value) && (window['search-list'] && window['search-list'].focus());
		},
	},
	// stp
	window.stp = {
		//// DEBUG
		// debug : 1,
		//// SETTINGS
		CLUSTER      : {true : 'uweb', }[ /^s7[0-9]{2}/.test(location.host) ] || 'ucoz',
		// regSERVER    : [43, 105, 702 ],
		vipServer    : [ 17, 32, 66, 69, 71, 74, 75, 76, 78, 81, 90, 92, 93, 98 ],
		autosubmitEA : 0,    // 0 = отключено, 500 = таймаут закрытия в миллисекундах (число может быть любым)
		showTraff    : 7,    // 0 = отключено, 7 = количество отображаемых строк (число может быть от 1 до 10)
		showLogin    : 7,    // 0 = отключено, 7 = количество отображаемых строк (число может быть от 1 до 10)
		modalDays    : [0, 1, 2, 4, 8, 16 ],
		timeout      : {
			closeWindow : 1500,
		},
		//// DATA
		payedLINKS : {
			// ключ         имя ссылки        комментарий                                           текст ошибки
			testing      : ['Тестирование',   'Тестирование - @@@. ',                               'Необходимо заполнить причину!' ],
			correction   : ['Сorrection',     'Коррекция баланса. @@@. ',                           'Необходимо заполнить причину!' ],
			uTemplate    : ['uTemplate',      'Оплата заказа @@@ uTemplate.pro. ',                  'Необходимо заполнить причину!' ],
			Refunds      : ['Refunds',        'Возврат средств по требованию пользователя - @@@. ', 'Необходимо заполнить причину!' ],
			PayPal       : ['PayPal',         'Прямая оплата на PayPal с @@@. ',                    'Необходимо указать email!' ],
			cancellation : ['Списание',       'Перевод средств на сайт @@@ по запросу в ТП. ',      'Необходимо заполнить адрес сайта!' ],
			enrollment   : ['Зачисление',     'Перевод средств с сайта @@@ по запросу в ТП. ',      'Необходимо заполнить адрес сайта!' ],
			cashless     : ['Безнал',         'Безнал по счету №@@@. ',                             'Необходимо в поле указать размер скидки!' ],
			discount     : ['Скидка студиям', 'Скидка на оплату услуг для партнера- @@@. ',         'Укажите в поле идентификатор партнера (имя / название студии)!' ],
		},
		whoisURL : {
			'.ua'       : 'dig.ua/search/',
			'.name'     : 'dig.ua/search/',
			'.by'       : 'dig.ua/search/',
			'.pro'      : 'dig.ua/search/',
			'.ru'       : 'nic.ru/whois/?query=',
			'.su'       : 'nic.ru/whois/?query=',
			'.xn--p1ai' : 'nic.ru/whois/?query=',
			default     : 'who.is/whois/',
		},
		uPackages : [
			//id sum     name
			[11, 28.70,  'Минимальный', ], //mini
			[12, 57.50,  'Базовый', ], //base
			[13, 76.70,  'Оптимальный', ], //opti
			[14, 153.50, 'Максимальный', ], //maxi
			[15, 95.90,  'Магазин', ], //shop
		],
	},
);
