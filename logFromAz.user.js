﻿// ==UserScript==
// @name 			logFromAz
// @description 	Логирование из админки
// @namespace 		logFromAz by masterwind
// @author 			masterwind
// @version 		15.10.9
// @include 		*//alog.ucozmedia.com/inner*
// @exclude 		*//docs.google.com/forms*/edit
// @include 		*//spreadsheets.google.com/spreadsheet/*
// @include 		*//docs.google.com/forms*
// @require 		http://yastatic.net/jquery/2.1.4/jquery.min.js
// @grant 			unsafeWindow
// @noframes
// ==/UserScript==

this.$ = this.jQuery = jQuery.noConflict(true);

var _SS_ 	= window.sessionStorage;
$('style:first').append('.main{padding:15px!important; }');

if (window.name.indexOf('siteNAME') > -1) {
	// 					siteNAME, action, reason, comment
	var STP 				= JSON.parse(window.name),
		 userSITES 		= $('.poparea[name="user_sites"]'),
		 userACTION 	= $('select[name="user_action"]'),
		 userREASON 	= $('#reason-area'),
		 userCOMMENT 	= $('textarea[name="user_comment"]');

	$('button.btn-success').on('click', function (e) {
		_SS_.setItem('itemADDED', '1');
	});

	if (_SS_.itemADDED === '1' && location.href.indexOf('?v=') > -1) {
		window.setTimeout(function () {
			window.close();
		}, 1000);
	}

	//САЙТ(Ы)
	userSITES.val(STP.siteNAME);
	//ДЕЙСТВИЕ
	switch (STP.action) {
		case 'del w/ block' 		: userACTION.val(1); 	break;
		case 'del w/o block' 	: userACTION.val(2); 	break;
		case 'block' 				: userACTION.val(3); 	break;
		case 'unblock' 			: userACTION.val(4); 	break;
		case 'restore' 			: userACTION.val(5); 	break;
		case 'banner' 				: userACTION.val(6); 	break;
	};
	//ПРИЧИНА
	if (STP.reason !== 'undefined') {
		userREASON.val(STP.reason);
	}
	//КОММЕНТАРИЙ
	if (STP.comment !== 'undefined') {
		userCOMMENT.val(STP.comment);
	}
	//фокус в поле "Комментарий"
	userCOMMENT.focus();
}


//if(window.name=='bannerGDlog'){
	if ($('body:contains("твет записан.")').length) {
		setTimeout(function () {
			window.close();
		}, 750);
	}
//}
