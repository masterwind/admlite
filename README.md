# README #

Скрипты для админки.

### Что здесь? ###

* admLITE.css - стили
* admLITE.user.js - скрипт для админки саппорта
* admLITEauth.user.js - скрипт для авторизации в админку
* logFromAz.user.js - скрипт логирования из админки //deprecated
* DOM.js - скрипт обработки системных поддоменов

### Как его? ###

Для работы скриптов необходимо установить дополнение в браузер.

* Firefox - Greasemonkey
* Chrome - Tampermonkey
* Opera 12x - Violentmonkey
