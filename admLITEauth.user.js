// ==UserScript==
// @name 			adminka lite authorization
// @description 	Авторизация в админку для саппорта
// @namespace 		adminka-lite-authorization-masterwind
// @author 			masterwind
// @version 		15.10.30
// @include 		http://*.uweb.ru/cgi/uAdmin/*
// @include 		http://*.ucoz.net/cgi/uAdmin/*
// @grant 			unsafeWindow
// @noframes
// ==/UserScript== */


/**
 * SETTINGS
 */
var _DOC_ 		= document,
	 _LS_ 		= window.localStorage,
	 _SS_ 		= window.sessionStorage,
	 login 		= _DOC_.querySelector('[value="login"][name="act"]');
_SS_.setItem('moder', JSON.stringify({NAME:'NAME', LOGIN:'LOGIN', PASS:btoa('PASSWORD'), AUTO:1 }));
var moder 		= JSON.parse(_SS_.moder);

/**
 * Заполнение полей формы авторизации
 * @return 	{undefined} 	функция ничего не возвращает, только заполняет форму
 */
function fillLOGINform() {
	_DOC_.querySelector('input[name="login"]').value 		= moder.LOGIN;
	_DOC_.querySelector('input[name="password"]').value 	= atob(moder.PASS);
}

/**
 * Автозаполнение формы входа
 */
if (login) {
	if (moder.LOGIN !== 'LOGIN' || atob(moder.PASS) !== 'PASSWORD') {
		fillLOGINform();
		if (moder.AUTO !== 0) {
			_DOC_.querySelector('form').submit();
		}
	} else {
		var warning 	= _DOC_.createElement('DIV');
		warning.setAttribute('style', 'margin:15px auto; width:60%; border:2px solid maroon; padding:7px; background:#ea9999; ');
		warning.innerHTML = 'Одно поле (или несколько) настроек скрипта не заполнено.<br>\
			Для корректной работы заполните поля или отключите функцию автовхода.<br>\
			<b>_SS_.setItem(\'moder\', JSON.stringify({NAME:\'NAME\', LOGIN:\'LOGIN\', PASS:btoa(\'PASSWORD\'), AUTO:1 }));</b><br>\
			NAME - Имя саппортера, подставляемое при блокировке или создании платежа<br>\
			LOGIN/PASSWORD - Логин/пароль саппортера в админку<br>\
			1/0 - включен/выключен автовход';
		_DOC_.querySelectorAll('td')[1].appendChild(warning);
	}
	// имя/логин запишем в сторадж
	_LS_.setItem('moderNAME', moder.NAME);
	_LS_.setItem('moderLOGIN', moder.LOGIN);
}

