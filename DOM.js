window._DOM = {
	0 : '.ucoz.ru',
	1 : '.vo.uz',
	2 : '.at.ua',
	3 : '.p0.ru',
	4 : '.3dn.ru',
	5 : '.my1.ru',
	6 : '.clan.su',
	7 : '.moy.su',
	8 : '.do.am',
	9 : '.pp.net.ua',
	A : '.dmon.com',
	B : '.ovo.bg',
	C : '.uweb.ru',
	D : '.webbuilder3d.com',
	a : '.ucoz.ua',
	b : '.ucoz.kz',
	c : '.ucoz.lv',
	d : '.ucoz.com',
	e : '.ucoz.net',
	f : '.ucoz.org',
	g : '.ucoz.co.uk',
	h : '.ucoz.de',
	i : '.ucoz.es',
	j : '.ucoz.hu',
	k : '.ucoz.ae',
	l : '.usite.pro',
	// l : '.ucoz.fr', // deprecated
	m : '.ucoz.ro',
	n : '.ucoz.pl',
	o : '.narod.ru',
	p : '.ucoz.com.br',
	q : '.narod2.ru',
	r : '.ucoz.site',
	// s : '.ucoz.in', // deprecated
	s : '.ucoz.club',
	t : '.clan.la',
	u : '.me.la',
	// v : '.____',
	// w : '.____',
	// x : '.____',
	// y : '.____',
	z : '.ucozmedia.com',
};

/**
 * Функция получает адрес сайта и возвращает юзернейм в системе
 * Пример: betatest-0707.ucoz.ru -> 0betatest-0707
 * @param     {string}    siteADRESS    системный адрес сайта
 * @return    {string}                  идентификатор сайта в системе
 */
window.ucozSITE2userDOM = (siteADRESS, DOM = siteADRESS.trim().match(/^(.+)(\..+\.\w{2,})$/) ) => {
	for (let L in _DOM ) if (DOM[2] == _DOM[L] ) return L + DOM[1];
};

/**
 * Функция может работать в двух режимах:
 * - получает идентификатор сайта в системе - возвращает адрес сайта (Пример: 0betatest-0707 -> betatest-0707.ucoz.ru )
 * - получает код системного поддомена - возвращает системный поддомен (Пример: 0 -> .ucoz.ru )
 * @param     {string}    userDOM    идентификатор сайта в системе либо код системного поддомена
 * @return    {string}               системный поддомен / адрес сайта
 */
window.userDOM2ucozSITE = (userDOM, L = userDOM.trim().match(/^(.{1})(.+)$/) ) => {
	userDOM = userDOM.trim();
	if (userDOM.length === 1 ) return _DOM[userDOM];
	return L[2] + _DOM[L[1]];
};
